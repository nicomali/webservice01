﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using WEBService01.Service;

namespace WEBService01
{
    class Program
    {
        static Service1 service;
        static ServiceHost host;
        static void Main(string[] args)
        {
            service = new Service1();
            host = new ServiceHost(typeof(Service1), new Uri("http://localhost:8081/Service1/"));
            LoadService();
            Console.ReadKey();
            host.Close();
        }

        static void LoadService()
        {
            
            host.Open();
        }
    }
}
